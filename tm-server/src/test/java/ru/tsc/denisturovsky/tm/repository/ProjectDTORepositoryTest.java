package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.denisturovsky.tm.api.service.IConnectionService;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.ITaskDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.comparator.NameComparator;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.repository.dto.ProjectDTORepository;
import ru.tsc.denisturovsky.tm.sevice.ConnectionService;
import ru.tsc.denisturovsky.tm.sevice.PropertyService;
import ru.tsc.denisturovsky.tm.sevice.dto.ProjectDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.TaskDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectDTORepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static IProjectDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDTORepository(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin(USER_TEST_LOGIN);
        if (user != null) userService.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(userId, USER_PROJECT3));
            entityManager.getTransaction().commit();
            @Nullable final ProjectDTO project = repository.findOneById(userId, USER_PROJECT3.getId());
            Assert.assertNotNull(project);
            Assert.assertEquals(USER_PROJECT3.getId(), project.getId());
            Assert.assertEquals(userId, project.getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Before
    public void before() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, USER_PROJECT1);
            repository.add(userId, USER_PROJECT2);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void clearByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
            Assert.assertEquals(0, repository.getSize(userId));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTO project = repository.create(userId, USER_PROJECT3.getName());
            Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
            Assert.assertEquals(userId, project.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserIdWithDescription() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTO project = repository.create(userId, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
            Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
            Assert.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
            Assert.assertEquals(userId, project.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(userId, USER_PROJECT1.getId()));
        entityManager.close();
    }

    @Test
    public void findAllByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<ProjectDTO> projects = repository.findAll(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
        entityManager.close();
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<ProjectDTO> projects = repository.findAll(userId, comparator);
        Assert.assertNotNull(projects);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
        entityManager.close();
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = repository.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
        entityManager.close();
    }

    @Test
    public void getSizeByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(2, repository.getSize(userId));
        entityManager.close();
    }

    @Test
    public void removeByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(userId, USER_PROJECT2);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER_PROJECT2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void update() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            USER_PROJECT1.setName(USER_PROJECT3.getName());
            repository.update(USER_PROJECT1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER_PROJECT3.getName(), repository.findOneById(userId, USER_PROJECT1.getId()).getName());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}